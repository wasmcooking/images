# Multi tools

- Try to build manually to reduce the delay of build and find a way to reduce the size of container image
- https://github.com/docker-slim/docker-slim

When you run `docker images`, sizes are **huges** 🤯

```bash
REPOSITORY                                           TAG       IMAGE ID       CREATED         SIZE
multi-tools                                          latest    052c9b0c822e   5 minutes ago   7.34GB
registry.gitlab.com/wasmcooking/images/multi-tools   0.0.0     052c9b0c822e   5 minutes ago   7.34GB
gitpod/workspace-full                                latest    1ab1fdcda67e   5 weeks ago     6.16GB
```

brew install docker-slim

### History

- `0.0.0` first build
