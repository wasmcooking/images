# images

All below environment variables must be set in your Gitpod profile

- `GITLAB_HANDLE`
- `GITLAB_TOKEN_ADMIN`
- `DOCKER_USER`
- `DOCKER_PWD`

Every image is defined in a directory, every directory contains a `.env` file with appropriate environment variables to build the images:

- `IMAGE_NAME`
- `IMAGE_TAG`

