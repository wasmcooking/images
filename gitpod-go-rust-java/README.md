wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.0.0.2/graalvm-ce-java17-linux-amd64-22.0.0.2.tar.gz
tar -xvzf graalvm-ce-java17-linux-amd64-22.0.0.2.tar.gz
sudo mkdir /usr/lib/jvm/
sudo mv graalvm-ce-java17-22.0.0.2 /usr/lib/jvm/
cd /usr/lib/jvm
sudo ln -s graalvm-ce-java17-22.0.0.2 graalvm


export JAVA_HOME=/usr/lib/jvm
export PATH=$PATH:$JAVA_HOME/bin


sudo echo "source /home/gitpod/.gvm/scripts/gvm" >> $HOME/.bashrc
sudo echo "gvm use go1.17.5" >> $HOME/.bashrc

. /home/gitpod/.sdkman/bin/sdkman-init.sh 
sdk install java 22.0.0.2.r17-grl 